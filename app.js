'use strict';

const express = require('express'),
  app = express(),
  bodyParser = require("body-parser"),
  admin = require('firebase-admin'),
  serviceAccount = require('./recipe-app-195017-firebase-adminsdk-awkfa-5deca5d91a.json'),
  newRecipe = require('./routes/newRecipe'),
  authRoute = require('./routes/auth');
let uid;

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: 'https://recipe-app-195017.firebaseio.com',
});

app.use(bodyParser.urlencoded({
  extended: false
})).use(bodyParser.json());

app.use([authRoute, newRecipe]);

// Start the server
const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
  console.log(`App listening on port ${PORT}`);
  console.log('Press Ctrl+C to quit.');
});