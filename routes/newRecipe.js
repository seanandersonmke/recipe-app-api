const express = require('express'),
  router = express.Router(),
  admin = require('firebase-admin');

router.post('/newrecipe', function(req, res) {
  res.status(200).json({
    "message": "new recipe added"
  });
});

module.exports = router;