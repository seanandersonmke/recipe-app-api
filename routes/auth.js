const express = require('express'),
  admin = require('firebase-admin');
let router = express.Router();

router.post('/auth', function(req, res) {
  admin
    .auth()
    .verifyIdToken(req.body.token)
    .then(function(decodedToken) {
      uid = decodedToken.uid;
      res.status(200).json({
        "message": "transaction complete"
      });
      // ...
    })
    .catch(function(error) {
      console.log(error);
      res.status(404).json({
        "error": "not found",
        "err": error
      });
    });
});

module.exports = router;