#Recipe App API

App.js is the entry point of the app. Starts the server and initializes Firebase API.

Routes are stored in the routes folder with 1 file per route.

The app accepts a Firebase authorization from the front end to authorize a user with the database.

App is hosted on a Google App Engine. The deployment will also run NPM install on the remote server when deployed. See package.json for deployment scripts.
